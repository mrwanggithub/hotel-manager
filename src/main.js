import { createApp } from 'vue'
import App from './App.vue'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'


createApp(App).use(store).use(ElementUI).mount('#app')
